from django.contrib import admin
from .models import TodoList, TodoItem


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_num_items')

    def get_num_items(self, obj):
        return obj.items.count()
    get_num_items.short_description = 'Number of items'


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = ('task', 'due_date', 'is_completed', 'list')
    list_filter = ('is_completed',)
